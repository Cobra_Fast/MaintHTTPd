MaintHTTPd
==========

Super simple webserver for maintenance situations.

# Requirements
PHP5-CLI available as 'php'.

# Running
Execute on your favourite *nix console:

```$ ./mainthttpd.php```

alernatively

```$ /path/to/php5 -f mainthttpd.php```

should work.
