#!/usr/bin/env php
<?php

$str = file_get_contents('503.html');
$port = 80;

echo 'MaintHTTPd starting up on port ' . $port . '...' . "\n";
echo 'Reponse string is: ' . $str . "\n";

$socket = stream_socket_server("tcp://0.0.0.0:$port", $errno, $errstr);
if (!$socket)
    die($errno . ': ' . $errstr . ' @' . __LINE__);
else
    echo 'Socket opened, listening for requests.' . "\n\n";
    
$strlen = strlen($str);
$start = time();
$i = 0;
while (true)
{
    if (is_resource($conn = @stream_socket_accept($socket)))
    {
        fwrite($conn, "HTTP/1.1 503 Service unavailable\r\nContent-Type: text/html; charset=utf-8\r\nContent-Length: " . $strlen . "\r\n\r\n" . $str . "\r\n\r\n");
        fclose($conn);
        echo "\r" . 'Served ' . ++$i . ' requests. (' . sprintf('%.2f', ($i/(time() - $start))) . '/s)   ';
    }
}

fclose($socket);
